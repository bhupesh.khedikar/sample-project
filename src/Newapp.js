import { useNavigate } from 'react-router-dom';

function Newapp() {

  const navigate=useNavigate()

  const adddata = () => {
    fetch('https://jsonplaceholder.typicode.com/users').then((res) => {
      res.json().then((result) => {
        navigate("/page2", {
          state:{result}
        })
      })
    })
  }

  return (
    <>
    <button onClick={adddata}>click me</button>
   </>
  );
}

export default Newapp;
